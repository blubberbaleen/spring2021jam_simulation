using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] 
public class singleParamEvent : UnityEvent<bool> { }

public class toggleTrigger : MonoBehaviour
{
    public bool playerIsInTrigger = false;
    public singleParamEvent e;

    private void Start()
    {
        InvokeEvent();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            playerIsInTrigger = true;
            InvokeEvent();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            playerIsInTrigger = false;
            InvokeEvent();
        }
    }

    private void InvokeEvent()
    {
        e.Invoke(playerIsInTrigger);
    }

}
