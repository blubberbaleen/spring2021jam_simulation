using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{

    [SerializeField] AudioMixer environmentMixer;
    [SerializeField] [Range(-80, 20)] float enviromixerVol;
    [Header("Fade Up On Awake?")]
    [SerializeField] bool fadeUpOnAwake = false;
    [SerializeField] float fadeTime = 3f;
    [SerializeField] float prewait = 0.5f;

    [Header("FX")]
    [SerializeField] GameObject clearingLimit;


    [SerializeField] GameObject player;

    string vol_enviro = "VOL_Enviro";
    string vol_lake = "VOL_Lake";
    string vol_tree = "VOL_Tree";
    string vol_birds = "VOL_Birds";

    string lpf_freq = "LPF_Freq";

    

    private void Start()
    {
        player = GameObject.Find("Player");

        environmentMixer.SetFloat(vol_enviro, -70f);
        /*environmentMixer.SetFloat(vol_lake, -80f);
        environmentMixer.SetFloat(vol_tree, -80f);
        environmentMixer.SetFloat(vol_birds, -80f);*/

        if (fadeUpOnAwake)
            StartCoroutine(WaitThenStartFade(prewait, environmentMixer, vol_enviro, enviromixerVol, fadeTime));

    }

    public void toggleClearingFilter(bool playerIsInClearing)
    {
        if (playerIsInClearing)
        {
            environmentMixer.SetFloat(lpf_freq, 22000f);
        }
        else 
        {
            environmentMixer.SetFloat(lpf_freq, 10000f);
        }

    }


    #region Fade

    void Fade(AudioMixer mixer, string param, float targetVol, float duration)
    {
        float startingVol = GetLevel(mixer, param);
        StartCoroutine(StartFade(mixer, param, startingVol, targetVol, duration));

    }

    IEnumerator StartFade(AudioMixer mixer, string param, float startingVol, float targetVol, float duration)
    {
        float currentTime = 0f;
        
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            mixer.SetFloat(param, Mathf.Lerp(startingVol, targetVol, currentTime / duration));

            yield return null;
        }

    }

    IEnumerator WaitThenStartFade(float timeToWait, AudioMixer mixer, string param, float targetVol, float duration)
    {
        yield return new WaitForSeconds(timeToWait);
        Fade(mixer, param, targetVol, duration);

    }


    private float GetLevel(AudioMixer mixer, string param)
    {
        /* credit: JeromeWork: *https://answers.unity.com/questions/988884/how-do-i-use-audiomixergetfloat.html* */
        float value;
        bool result = mixer.GetFloat(param, out value);
        if (result)
            return value;
        else
            return 0f;
    }
    #endregion

}
