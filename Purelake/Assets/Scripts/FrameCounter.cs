/* This script was original part of the UnityStandardAssets package. */

using UnityEngine;

public class FrameCounter : MonoBehaviour
{
    [SerializeField] float measurementPeriod = 3.0f;  // seconds
    private float t_next = 0;
    private int currentFPS;
    private int k;

    private void Start()
    {
        t_next = Time.realtimeSinceStartup + measurementPeriod;
    }

    private void Update()
    {
        k++;
        if (Time.realtimeSinceStartup > t_next)
        {
            currentFPS = (int)(k / measurementPeriod);
            k = 0;
            t_next += measurementPeriod;
            Debug.Log("Frames Per Second:  " + currentFPS);
        }
    }
}

