using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float sightRange = 5.0f;
    //[SerializeField] float turnSpeed = 5.0f;

    NavMeshAgent navMeshAgent;
    private float distanceToTarget = Mathf.Infinity;

    private Vector3 vecDebug;
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        distanceToTarget = Vector3.Distance(target.position, transform.position);
        if (distanceToTarget < sightRange)
        {
            Vector3 rHat = (target.position - transform.position).normalized;
            //Quaternion lookRotation = Quaternion.LookRotation(new Vector3(rHat.x, rHat.y - 90, rHat.z));
            //transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);

            vecDebug = rHat;

            navMeshAgent.SetDestination(target.position);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, sightRange);
        Gizmos.DrawRay(target.position, 15 * vecDebug);
    }
}

